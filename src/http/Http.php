<?php
namespace Ysian\Tools\http;

class Http
{
    /**
     * @desc get请求
     * @param $url
     * @return bool|string
     */
    public static function curl_get($url)
    {
        #1  测试一 1

        //注释添加qqqafjdsjaaaa1111
        $ch = curl_init(); //创建一个新cURL资源

        //s设置选项,包括url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

        //执行并获取HTML文档内容
        $output = curl_exec($ch);
        curl_close($ch);//释放curl句柄
        return $output;
    }

    /**
     * @desc post请求
     * @param $url
     * @param $data
     * @return bool|string
     */
    public static function curl_post($url ,$data)
    {
        if(is_array($data)) $data = json_encode($data);
        $ch = curl_init(); //创建一个新cURL资源
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); //忽略证书验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch); // 抓取URL并把它传递给浏览器
        curl_close($ch); // 关闭cURL资源，并且释放系统资源
        return $output;
    }
}