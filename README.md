工具类

##目录
    
    一.file
        1.File.php

##使用方法

    目录一
        1.File.php文件 
        使用方法:在文件中引入 use Ysian\Tools\file\File;

        (1).download()方法,作用 下载一个远程图片 
        /**
         * @param $url 远程地址(必填) 例如:https://cdn1.xunkyz.com/tocConsul/tocFile/1629696719345_img_tongdian@2x.png
         * @param string $filename(非必填,不填一时间戳作为保存的文件名) 保存的文件名称
         * @param string $save_dir(非必填,不填保存在根目录) 文件保存目录
         * @return string 返回的是文件保存路径 例:./1648630147.png
         * @throws \Exception 文件路径不存在且没有创建文件的权限,则抛出 文件目录不存在 的警示
         */
         public function download()
         {
             $url = 'http://weiwoju.oss-cn-hangzhou.aliyuncs.com/image/spMarket/logo.png';
             $path = File::download($url);
         }
            
        (2).fileIsExist()方法,作用是检测本地文件或者远程文件是否存在
         /**
         * @param $file 文件路径或者url地址,存在返回true,否则抛出异常
         * @return bool
         * @throws \Exception
         */
        使用 File::fileIsExist($file)


